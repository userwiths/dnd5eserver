This is the repository that is used by [D&D 5e client app](https://gitlab.com/userwiths/dnd5eclient).  

It Depends on the following programs:

>* Node.js
>* MongoDB.
>* NPM package manager.

After starting the MongoDB server, you can start the server by running  
>node server.js  

The server will write out the port its running on and a large gibberish,  
that is meant to be send to the people supposed to be able to connect to the current server.