function namedList(items){
    return items.map((item)=>{
        return {
            name:item.name,
            url:item.url
        };
    });
};

module.exports = namedList;