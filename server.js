const express = require('express');
const bodyParser=require('body-parser');
const jwt = require('jsonwebtoken');
const externalip=require('externalip');
const cors = require('cors');

const characterRoutes=require('./routers/characterRouter');
const equipmentRoutes=require('./routers/equipmentRouter');
const equipmentCategoriesRoutes=require('./routers/equipmentCategoryRouter');
const baseRoutes=require('./routers/baseRouter');

const app = express();
const port = 4665;

let token={};

let corsOptions = {
  origin: '*',
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204 
};

function tokenAddress(){
    let tokenItem;
    externalip(function (err, ip) {
        tokenItem={'address':ip+':'+port};
        tokenItem=jwt.sign(tokenItem,'secret',
        {
            expiresIn: '24h' // expires in 24 hours
        });
        console.log(tokenItem);
        setTimeout(()=>token=tokenItem,500);
    });
    return tokenItem;
}

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(cors(corsOptions));

app.use('/',baseRoutes);
app.use('/characters',characterRoutes);
app.use('/equipment',equipmentRoutes);
app.use('/equipment-categories',equipmentCategoriesRoutes);

app.listen(port, () => {
    console.log(`DnD Manager listening on port ${port}!`);
    tokenAddress();
});

