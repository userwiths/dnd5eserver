const express = require('express');
const mongo= require('mongodb').MongoClient;
const utils=require('../utils');

let equipmentRouter = express.Router();
const url = 'mongodb://127.0.0.1:27017';
  
equipmentRouter.post('/', (req, res) => {
    mongo.connect(url).
    then((db)=>{
      let item=req.body;
      let collection=db.collection('equipment');
      collection.find().toArray().
      then((docs)=>{
        if(docs.length===0){
          item.index=1;
        }else{
          item.index=docs[docs.length-1].index+1;
        }

        item.url='equipment/'+item.index;

        collection.insert(item);
        res.json(item);
      }).catch((err)=>{console.log("Failed item retrieval");});
    }).
    catch((err)=>console.log("Failed connection"));
});
  
equipmentRouter.get('/', (req, res) => {
    mongo.connect(url).then((db)=>{
      let collection=db.collection('equipment');
      collection.find().toArray().
      then((docs)=>{
        res.json(utils(docs));
      }).
      catch((err)=>{console.log(err);});
    }).catch((err)=>console.error(err));
});
  
equipmentRouter.get('/:index', (req, res) => {
    mongo.connect(url).then((db)=>{
        let collection=db.collection('equipment');
        collection.find().toArray().
        then((docs)=>{
            if(docs.length>req.params.index && req.params.index>=0){
                res.json(docs[req.params.index]);
            }else{
                res.json({code:404,text:"Not Found"});
            }
        }).
        catch((err)=>{console.log(err);});
    });
});
  
equipmentRouter.put('/:index', (req, res) => {
    let collection;
  
    mongo.connect(url).then((db)=>{
      collection=db.collection('equipment');
      collection.update({index:req.params.index},{"$set":req.body}).
        fail((err)=>console.error("Failed to Update "+req.params.index));
      res.json({code:201,text:"Updated entry"});
    }).catch((err)=>{console.log(err);});
});
  
equipmentRouter.delete('/:index',(req,res)=>{
    mongo.connect(url).then((db)=>{
      let collection=db.collection('equipment');
      collection.remove({index:parseInt(req.params.index)},function(err,result){
        if(err){
          res.json({code:500,error:err});
        }else{
          res.json({deleted_items:result.result.n});
        }
      });
    }).catch((err)=>{console.error(err);});
});

module.exports = equipmentRouter;