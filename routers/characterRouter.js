const express = require('express');
const mongo= require('mongodb').MongoClient;


let characterRouter = express.Router();
const url = 'mongodb://127.0.0.1:27017';
  
characterRouter.post('/', (req, res) => {
    mongo.connect(url).
    then((db)=>{
      let item=req.body;
      let collection=db.collection('characters');
      collection.find().toArray().
      then((docs)=>{
        if(docs.length===0){
          item.index=1;
        }else{
          item.index=docs[docs.length-1].index+1;
        }
        
        item.url='characters/'+item.index;
        
        collection.insert(item);
        res.json(item);
      }).catch((err)=>{console.log('Failed item retrieval');});
    }).
    catch((err)=>console.log('Failed connection'));
});
  
characterRouter.get('/', (req, res) => {
    mongo.connect(url).then((db)=>{
      let collection=db.collection('characters');
      collection.find().toArray().
      then((docs)=>{
        res.json(docs);
      }).
      catch((err)=>{console.log(err);});
    }).catch((err)=>console.error(err));
});
  
characterRouter.get('/:index', (req, res) => {
    mongo.connect(url).then((db)=>{
        let collection=db.collection('characters');
        collection.find().toArray().
        then((docs)=>{
            if(docs.length>req.params.index && req.params.index>=0){
                res.json(docs[req.params.index]);
            }else{
                res.json({code:404,text:'Not Found'});
            }
        }).
        catch((err)=>{console.log(err);});
    });
});
  
characterRouter.put('/:index', (req, res) => {
    let id;
    let collection;
  
    mongo.connect(url).then((db)=>{
      collection=db.collection('characters');
      collection.update({index:req.params.index},{'$set':req.body}).
        fail((err)=>console.error('Failed to Update '+req.params.index));
      res.json({code:201,text:'Updated entry'});
    }).catch((err)=>{console.log(err);});
});
  
characterRouter.delete('/:index',(req,res)=>{
    mongo.connect(url).then((db)=>{
      let collection=db.collection('characters');
      collection.remove({index:req.params.index},function(err,result){
        if(err){
          console.error(err);
        }else{
          console.log('Index: '+req.params.index);
          console.log('Deleted: '+result.result.n);
        }
      });
    }).catch((err)=>{console.error(err);});
    res.json({text:'Deleted Item with index: '+req.params.index});
});

module.exports = characterRouter;