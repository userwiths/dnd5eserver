const express = require('express');
const mongo= require('mongodb').MongoClient;
const utils=require('../utils');

let baseRouter = express.Router();
  
baseRouter.get('/', (req, res) => {
    res.json({code:200,text:"Connected to GM's host."});
});

module.exports = baseRouter;