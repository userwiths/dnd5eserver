const express = require('express');
const mongo= require('mongodb').MongoClient;
const utils= require('../utils');

let equipmentCategoryRouter = express.Router();
const url = 'mongodb://127.0.0.1:27017';
  
equipmentCategoryRouter.get('/', (req, res) => {
    res.json(
      [
        {name:'Weapons',url:'equipment-categories/1'},
        {name:'Armors',url:'equipment-categories/2'},
        {name:'Tools',url:'equipment-categories/3'}
      ]
    );
});
  
equipmentCategoryRouter.get('/:index', (req, res) => {
    let keyWord;
    switch(req.params.index){
      case '1':keyWord='Weapon';break;
      case '2':keyWord='Armor';break;
      case '3':keyWord='Tools';break;
      default:res.json({code:404,text:'No such index.'});break;
    }
    mongo.connect(url).then((db)=>{
        let collection=db.collection('equipment');
        collection.find().toArray().
        then((docs)=>{
            docs=docs.filter((x)=>x.equipment_category===keyWord);
            res.json(utils(docs));
        }).
        catch((err)=>{console.log(err);});
    });
});

module.exports = equipmentCategoryRouter;